package general;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utils.QCloudUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class QCloudTest {
    private static QCloudUtils qCloudUtils;

    @BeforeClass
    public static void  setup() throws IOException {
        InputStream inputStream=QCloudUtils.class.getResourceAsStream("/config.properties");
        Properties properties=new Properties();
        properties.load(inputStream);

        String secretId=properties.getProperty("secretId");
        String secretKey=properties.getProperty("secretKey");
        String region=properties.getProperty("region");;
        String bucketName=properties.getProperty("bucketName");
        qCloudUtils=new QCloudUtils(secretId,secretKey,region);
        qCloudUtils.setupMongoClient("mongouser","1qaz2wsX","admin");
    }

    @Test
    public void listBucketTest(){
        qCloudUtils.listObjects("adas-sh-1253431691");

    }


    @Test
    public void moveObject(){
        String bucketName="adas-sh-1253431691";
        String objectKey="road-test/ES8/LJ1EEAUU0J7700325_20180612T084202";

        qCloudUtils.moveObjectsWithPrefix(bucketName,objectKey);
    }

}
