package general

import java.util.Properties
import java.util.logging.Logger

import utils.QCloudUtils

import scala.collection.JavaConverters._

object CloudTest {

  val logger=Logger.getLogger("CloudTest")

  def main(args: Array[String])={

    logger.info("Starting processing!")
    val inputProperties=this.getClass.getResourceAsStream("/config.properties")

    inputProperties match {
      case null => println("Usage: Please specify a config file in the classpath!")
      case _ => println("Using Properties:\n" )
    }

    val properties=new Properties()
    properties.load(inputProperties)
    for ((key,val2) <- properties.asScala){
      println(key + "\t" + val2)
    }

    val secretId=properties.getProperty("secretId");
    val secretKey=properties.getProperty("secretKey");
    val region=properties.getProperty("region");;
    val utilTestor=new QCloudUtils(secretId,secretKey,region);
    val bucketName=properties.getProperty("bucketName");
//    utilTestor.getObject("choices-1256246605","elasticsearch-6.2.3.tar.gz","D:/qcloud/data/fromTencent.tar.gz");

//    Range(1,11,1).foreach(
//      x =>
//      utilTestor.uploadObject(bucketName,"elasticsearch-6.2.3.tar.gz",bucketName,"elasticSearch-"+x+".tar.gz"))

//    utilTestor.listObjectsWithPrefix("adas-sh-1253431691","road-test/ES8/")
//      val key="road-test/ES8/LJ1EEAUU0J7700325_20180612T092212_data.tar.gz"
    val key="road-test/ES8/LJ1EEAUU0J7700325_20180612T091711_video.mp4"
    val newKey=utilTestor.transformKey(key)
    println(key + "\t=>\t" + newKey)

  }


  def uploadFile(fileName:String)={

  }


}
