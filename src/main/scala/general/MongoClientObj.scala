package general

import com.mongodb.{MongoClient, MongoClientOptions, MongoCredential, ServerAddress}

import scala.collection.JavaConverters._
object MongoClientObj extends  App{

  val host="10.132.18.17"
  val port=27017
  val userName="mongouser"
  val password="1qaz2wsX"
  val database="admin"
  val userDatabase="adas-dev"
  val credential=MongoCredential.createCredential(userName,database,password.toCharArray)
//  val connectionOptions=MongoClientOptions.builder().sslEnabled(true).build()
  val mongoClient=new MongoClient(new ServerAddress(host,port),List(credential).asJava)

  val databases=mongoClient.listDatabases()
  databases.asScala.foreach(x => println(x.toJson))


}
