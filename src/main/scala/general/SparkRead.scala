package general

import java.io.File
import java.util.Properties

import org.apache.spark.{SparkConf, SparkContext}
import utils.{CompressUtils, QCloudUtils}

import scala.collection.JavaConverters._
import scala.collection.mutable


object SparkRead {


  def main(args: Array[String])={
/*
Some settings for Tencent Cloud SDK API
 */

    val inputProperties=this.getClass.getResourceAsStream("/config.properties")

    inputProperties match {
      case null => println("Usage: Please specify a config file in the classpath!")
      case _ => println("Using Properties:\n" )
    }

    val properties=new Properties()
    properties.load(inputProperties)

    for ((key,val2) <- properties.asScala){
      println(key + "\t" + val2)
    }
    val settings=properties.asScala


//    System.exit(1)


    //Prepare Spark Job
    val conf=new SparkConf().setAppName("ReadCosFile").setMaster("local[2]")
    val sc=new SparkContext(conf)

    val fileList=Range(0,5,1)
    fileList.foreach(println)
    val rdd=sc.parallelize(fileList)

    //Start to extract files needed for processing
    val allFiles=rdd.map(x => processAndExtractTar(settings,x)).cache()

    //Retrieve different files as needed!
    val datFiles=allFiles.map(x => x.filter(y => y.endsWith(".dat")))

    val txtFiles=allFiles.map(x => x.filter(y => y.endsWith(".csv") || y.endsWith(".dbc")))
    println("Text Files:\n")
    datFiles.foreach(x => println(x.mkString(",\t")))
    txtFiles.foreach(x => println(x.mkString(",\t")))
/*
    Do whatever processing needed on the different RDD
*/

    sc.stop()

  }

  def processAndExtractTar(settings:mutable.Map[String,String], fileIndex:Int):List[String]={
    val bucketName=settings.get("bucketName") match{
      case None => {println("No bucket name specified"); System.exit(1); ""}
      case name:Some[String]  => name.get
    }
    val fileNames=List(
      "road-test/ES8/LJ1EEAUU0J7700325_20180612T123633_data.tar.gz",
      "road-test/ES8/LJ1EEAUU0J7700325_20180612T114257_data.tar.gz",
      "road-test/ES8/LJ1EEAUU0J7700325_20180612T113756_data.tar.gz",
      "road-test/ES8/LJ1EEAUU0J7700325_20180612T124635_data.tar.gz",
      "road-test/ES8/LJ1EEAUU0J7700325_20180612T125136_data.tar.gz"
    )
    val cosClient=new QCloudUtils(settings.getOrElse("secretId",""),settings.getOrElse("secretKey",""),settings.getOrElse("region",""))

    val fileName=fileNames(fileIndex)
    val tmpFile=File.createTempFile("qcloud",".tar.gz")
    val tmpFileName=tmpFile.getName
    cosClient.getObject(bucketName,fileName,tmpFile.getAbsolutePath)
    val tmpFolder=new File("d:/data/tmpFolder/" + fileIndex);
    if (!tmpFolder.exists()){
      tmpFolder.mkdirs()
    }
    val decompressor=new CompressUtils();

    val fileList=decompressor.decompress(tmpFile.getAbsolutePath,tmpFolder.getAbsolutePath)

//    val fileList=
    cosClient.closeConnection()

    return fileList.asScala.toList
  }

}
