package general

/**
  * Created by cai xingliang on 2/7/2017.
  */

object TestRuner extends App{

  println(List(1,2,3))


  val range= List.tabulate(10)( x => x)
  range.foreach( x =>  println(testAssoc) )

  val ll= for {x <- range} yield (x)
  println(ll)
  def f(u: Double, v: Double): Double =
    (u + v)/(1.0 + u*v)
  def err(lst:List[Double]): Double =
    lst.reduceLeft(f) - lst.reduceRight(f)
  def testAssoc: Double = {
    val r = new scala.util.Random
    val lst = List.fill(400)(r.nextDouble*0.002)
    err(lst)
  }
}


