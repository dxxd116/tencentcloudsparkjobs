package general

import java.util.Properties
import java.util.logging.{Level, Logger}

import play.api.libs.json.{JsArray, Json}
import utils.QCloudUtils

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.io.Source

object MoveCOSFiles  {

  val logger=Logger.getLogger("CloudTest")


  def main(args:Array[String])= {

    logger.info("Preparation for processing!")
    val inputProperties=this.getClass.getResourceAsStream("/config.properties")

    inputProperties match {
      case null => println("Usage: Please specify a config file in the classpath!")
      case _ => println("Using Properties:" )
    }

    val properties=new Properties()
    properties.load(inputProperties)
    for ((key,val2) <- properties.asScala){
      println(key + "\t" + val2)
    }
    println()

    val secretId=properties.getProperty("secretId");
    val secretKey=properties.getProperty("secretKey");
    val region=properties.getProperty("region");;
    val qCloudUtils=new QCloudUtils(secretId,secretKey,region);
    val bucketName=properties.getProperty("bucketName");

    qCloudUtils.setupMongoClient("mongouser", "1qaz2wsX", "admin")


    logger.info("Starting processing!")
//    qCloudUtils.listObjects("adas-sh-1253431691")
    val vins=getVinList().foreach( (x :String)=> {

      val vin="road-test/ES8/" + x + "_"
      println(vin)
      val count=qCloudUtils.moveObjectsWithPrefix(bucketName,vin)
      logger.info("Moved "+ count + " objects for " + vin)

    })

  }



    def getVinList(): List[String] ={
      val listBuffer=new ListBuffer[String]()
      val jsonStream=Source.fromInputStream(this.getClass.getResourceAsStream("/vins.json") ).mkString
      val jsonList=Json.parse(jsonStream)
//      jsonList.
      jsonList match {
        case array:JsArray => array.value.foreach( json =>
          {
            val vin=(json \ "vin").as[String]
            listBuffer += vin
          })
        case _ => logger.log(Level.SEVERE, "VIN file does not contain an array of vehicles!")
      }

//      listBuffer += "fdsa"
      return listBuffer.result()
//      return List("LJ1EEAUU9J7700629")
    }









}
