package utils;

import lombok.Data;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Data
public class CompressUtils {

    public List<String> decompress(String tarFile, String outputFolder){
//        filePath="D:/qcloud/data/fromQcloud.tar.gz";
//        String outputPath="D:/qcloud/data/uncompressed";
        List<String> files=new ArrayList<>();
        try (TarArchiveInputStream fin = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(tarFile)))){
            TarArchiveEntry entry;
            while ((entry = fin.getNextTarEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File curfile = new File(outputFolder, entry.getName());
//                System.out.println(entry.getName());

                files.add(entry.getName());
                File parent = curfile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                IOUtils.copy(fin, new FileOutputStream(curfile));

            }

        }catch (IOException e){
            e.printStackTrace();
        }

        return files;

    }
}
