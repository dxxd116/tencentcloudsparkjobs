package utils;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.utils.StringUtils;
import lombok.Data;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public  class QCloudUtils {

    private String secretId;
    private String secretKey;
    private String region;

    private COSClient cosClient;
    private MongoUtils mongoClient;
    String mongoDatabaseName="adas-dev";
    String mongoCollectionName="moveTencentFileRecords";

    private static final Logger logger=Logger.getLogger(QCloudUtils.class.getName());

    static FileHandler fh;
    static {
        try {

            // This block configure the logger with handler and formatter
            fh = new FileHandler("./moveTencentFiles"+ LocalDate.now().toString()+".log",true);


            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            // the following statement is used to log any messages
//            logger.info("My first log");

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException occured!");
            e.printStackTrace();
        }

//        logger.info("Hi How r u?");
    }
    public QCloudUtils(String secretId,String secretKey,String region){

        this.secretId=secretId;
        this.secretKey=secretKey;
        this.region=region;
        this.setUp();

    }

    public void setupMongoClient(String userName, String password, String database){
        this.mongoClient=new MongoUtils(userName,password,database);
    }


    public void setUp(){
// 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
// 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        ClientConfig clientConfig = new ClientConfig(new Region(region));
// 3 生成cos客户端
        this.cosClient = new COSClient(credentials, clientConfig);
// bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
    }


    public void getObject(String bucketName,String cosFile,String localFile){



// bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
//        String bucketName = "choices-1256246605";

//        String key="elasticsearch-6.2.3.tar.gz";
//        File file=new File("D:/qcloud/data/fromQcloud.tar.gz");
        File file=new File(localFile);

        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, cosFile);
        try {
            ObjectMetadata downObjectMeta = cosClient.getObject(getObjectRequest, file);
            System.out.println(downObjectMeta.getContentLength() + " bytes donwloaded!");
        } catch (CosServiceException e){
            e.printStackTrace();
        } catch (CosClientException e ){
            e.printStackTrace();
        }


//        cosClient.shutdown();

    }

    public void deleteObject(String bucketName,String objectKey){
        try {
            cosClient.deleteObject(bucketName,objectKey);
        } catch (CosServiceException e){
            logger.log(Level.SEVERE,"Fail to delete object:" + objectKey);
            System.out.println(e.getMessage());
        } catch (CosClientException e){
            e.printStackTrace();
        }

    }

    public String copyObject(String srcBucketName,String sourceKey,String destionationBucketName,String destinationKey){
        CopyObjectRequest copyObjectRequest=new CopyObjectRequest(srcBucketName,sourceKey,destionationBucketName,destinationKey);
        try {
            CopyObjectResult copyObjectResult=cosClient.copyObject(copyObjectRequest);
            return copyObjectResult.getDateStr();
        }
        catch (CosServiceException e){
            logger.log(Level.SEVERE,e.getErrorMessage());
            System.out.println(e.getMessage());
        }catch (CosClientException e){
            System.out.println(e.getMessage());
        }

        return "Error in copying Tencent object!";
    }

    public void listObjects(String bucketName){
        listObjectsWithPrefix(bucketName,"");
    }

    public void listObjectsWithPrefix(String bucketName,String prefix){
        ListObjectsRequest listObjectsRequest=new ListObjectsRequest(bucketName,prefix,"","",100);
        ObjectListing objectListing = null;
        boolean isTruncated = true;
        int i=0;
        while (isTruncated && i < 1000) {
            try {
                objectListing = cosClient.listObjects(listObjectsRequest);
                String marker = objectListing.getNextMarker();
                listObjectsRequest=new ListObjectsRequest(bucketName,prefix,marker,"",100);
                isTruncated=objectListing.isTruncated();



            } catch (CosServiceException e) {
                e.printStackTrace();
            } catch (CosClientException e) {
                e.printStackTrace();
            }

            List<String> commonPrefixs = objectListing.getCommonPrefixes();
            if (commonPrefixs.size() == 0) {
                System.out.println("Common prefixes is empty!");
            }
            commonPrefixs.forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    System.out.println(s);
                }
            });

            // object summary表示所有列出的object列表
            List<COSObjectSummary> cosObjectSummaries = objectListing.getObjectSummaries();
            for (COSObjectSummary cosObjectSummary : cosObjectSummaries) {
                // 文件的路径key
                String key = cosObjectSummary.getKey();
                // 文件的etag
                String etag = cosObjectSummary.getETag();
                // 文件的长度
                long fileSize = cosObjectSummary.getSize();
                // 文件的存储类型
                String storageClasses = cosObjectSummary.getStorageClass();
                i++;
                System.out.println("File number " + i + ":\t" + StringUtils.join("\t", "key:", key, "\tsize:", fileSize + ""));
            }
            //i+= cosObjectSummaries.size();
        }

    }

    public void uploadCopyObject(String srcBucketName,String srcKey, String destBucketName,String destKey){
        CopyObjectRequest copyObjectRequest=new CopyObjectRequest(srcBucketName,srcKey,destBucketName,destKey);
        CopyObjectResult copyObjectResult=cosClient.copyObject(copyObjectRequest);
    }

    public int moveObjectsWithPrefix(String bucketName,String prefix){

        ListObjectsRequest listObjectsRequest=new ListObjectsRequest(bucketName,prefix,"","",100);
        ObjectListing objectListing = null;
        boolean isTruncated = true;
        int i=0;
        List<Map<String,Object>> outputs=new ArrayList<>();
        while (isTruncated ) {
            try {
                objectListing = cosClient.listObjects(listObjectsRequest);
                String marker = objectListing.getNextMarker();
                listObjectsRequest=new ListObjectsRequest(bucketName,prefix,marker,"",100);
                isTruncated=objectListing.isTruncated();

            } catch (CosServiceException e) {
                e.printStackTrace();
            } catch (CosClientException e) {
                e.printStackTrace();
            }


            // object summary表示所有列出的object列表
            List<COSObjectSummary> cosObjectSummaries = objectListing.getObjectSummaries();
            logger.info("There are " + cosObjectSummaries.size() + " objects found for " + prefix + "!");

            for (COSObjectSummary cosObjectSummary : cosObjectSummaries) {
                // 文件的路径key
                String key = cosObjectSummary.getKey();
                // 文件的etag
                String etag = cosObjectSummary.getETag();
                // 文件的长度
                long fileSize = cosObjectSummary.getSize();
                // 文件的存储类型

                i++;
                System.out.println("File number " + i + ":\t" + StringUtils.join("\t", "key:", key, "\tsize:", fileSize + ""));
                logger.info("Moving " + key);
                String newKey=transformKey(key);
                String dateString=copyObject(bucketName, key,bucketName,newKey);
                logger.info("Object copied: " + newKey + "\t" + dateString);
                if (dateString.startsWith("Error")){ continue;}

                deleteObject(bucketName,key);
                logger.info("Object deleted: " + key);
                //Prepare to Update MongoDB
                HashMap<String,Object> result=new HashMap<>();
                result.put("bucketName", bucketName);
                result.put("object-key", key);
                result.put("copiedDateString", dateString);
                result.put("timestamp", System.currentTimeMillis() / 1000);
                outputs.add(result);
            }

         //Update mongoDB
            logger.info("Writting copy results to mongodb: " + StringUtils.join("\t",mongoDatabaseName,mongoCollectionName));
            mongoClient.insertDocuments(mongoDatabaseName,mongoCollectionName,outputs);
            outputs.clear();
            logger.info("Writting copy results to mongodb completed!");

        }
        return i;

    }

    public String transformKey(String key){
        String s;
            Pattern p=Pattern.compile("([a-zA-Z0-9-]+/\\w+/)(([A-Z0-9]+)_(\\d+)T(\\d{2}).*)");
            Matcher m= p.matcher(key);
            if (m.find()){
                String vin=m.group(3);
                String date=m.group(4);
                String hour=m.group(5);
                String fileName=m.group(2);
                s = StringUtils.join("/",vin,date,hour,fileName);
                s = m.group(1) +s;
            }else{
                logger.log(Level.SEVERE,"Regular expression fail to match the object key: " + key);
                return null;
            }


        return s;
    }

    public void closeConnection(){
        if (this.cosClient!=null){
            cosClient.shutdown();
        }
    }




}
