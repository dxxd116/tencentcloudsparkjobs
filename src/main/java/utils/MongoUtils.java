package utils;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.connection.ClusterSettings;
import lombok.Data;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
public class MongoUtils {
    private String userName;
    private String password;
    private String database;
    private String host="10.132.18.17";
    private Integer port=27017;
    private MongoClient client;

public static void main(String[] args){
    MongoClient client=MongoClients.create("mongodb://mongouser:1qaz2wsX@10.132.18.17:27017/admin");
    for (String name: client.listDatabaseNames()){
        System.out.println(name);
    }
}

    public MongoUtils(String userName, String password, String database) {
        this.userName = userName;
        this.password = password;
        this.database = database;

        MongoCredential credential=MongoCredential.createCredential(userName,database,password.toCharArray());
        ClusterSettings clusterSettings=ClusterSettings.builder().hosts(Arrays.asList( new ServerAddress(host,port))).build();
        MongoClientSettings clientSettings=MongoClientSettings.builder().applyToClusterSettings( builder ->
                builder.hosts(Arrays.asList(
                        new ServerAddress(host,port)
                ))
                )
                .credential(credential).build();

        this.client= MongoClients.create(clientSettings);
    }

    public MongoClient getClient(){
        return  this.client;
    }

    public void readDocuments(String databaseName,String collectionName){
        MongoDatabase mongoDatabase= client.getDatabase(databaseName);
        MongoCollection<Document> collection=mongoDatabase.getCollection(collectionName);

    }

    public void insertDocument(String databaseName, String collectionName,Document document){
        client.getDatabase(databaseName).getCollection(collectionName).insertOne(document);
    }

    public void insertDocuments(String databaseName, String collectionName, List<Map<String,Object>> jsonDocuments){
        List<Document> documents=new ArrayList<>();
        for (Map<String,Object> json: jsonDocuments){
            Document document=new Document(json);
            documents.add(document);
        }
        if (documents.size()==0){
            System.out.println("No documents to be processed!");
            return;
        }
        client.getDatabase(databaseName).getCollection(collectionName).insertMany(documents);
    }


}
