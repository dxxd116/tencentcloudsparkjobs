name := "scalaTest"

version := "1.0"

scalaVersion := "2.11.1"

dependencyOverrides += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.4"

// https://mvnrepository.com/artifact/com.tencentcloudapi/tencentcloud-sdk-java
libraryDependencies += "com.tencentcloudapi" % "tencentcloud-sdk-java" % "3.0.1"

// https://mvnrepository.com/artifact/com.qcloud/cos_api
libraryDependencies += "com.qcloud" % "cos_api" % "5.4.0"

// https://mvnrepository.com/artifact/org.apache.commons/commons-compress
libraryDependencies += "org.apache.commons" % "commons-compress" % "1.18"


libraryDependencies += "org.projectlombok" % "lombok" % "1.16.16"

// https://mvnrepository.com/artifact/org.apache.spark/spark-core
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.0"

// https://mvnrepository.com/artifact/junit/junit
libraryDependencies += "junit" % "junit" % "4.12" % Test

// https://mvnrepository.com/artifact/org.mongodb/mongo-java-driver
libraryDependencies += "org.mongodb" % "mongo-java-driver" % "3.7.0"

// https://mvnrepository.com/artifact/com.typesafe.play/play-json
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.10"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.3.6"
